const expres = require("express");
const  Article = require("../models/Articles")

const getArticle = (req, res, next) => {
  Article.find((err,data)=>{
    res.json({article:data});
  })
  
};

const createArticle = (req, res, next) => {
  const newArticle = new Article({
    title:req.body.title,
    content:req.body.content
  })
  newArticle.save((err,data)=>{
    res.json({article:data});
  })
  
};

const getArticleById = (req,res,next)=>{
Article.find({},(err,data)=>{
  data.forEach(item => {
    if( req.params.id === item.id ){
      res.json({article:{
        title:item.title,
        content:item.content
      }})
    }
  });
})
}

exports.getArticle = getArticle;
exports.createArticle = createArticle
exports.getArticleById = getArticleById
