const express = require('express');
const router = express.Router();
const userController = require("../controllers/userController")

/* GET users listing. */
router.get("/login", userController.Login);
router.get("/register", userController.Register);

module.exports = router;
