const express = require("express");
const router = express.Router();
const indexController = require("../controllers/indexController");

/* GET home page. */
router.get("/", indexController.getArticle);
router.get("/articles/:id", indexController.getArticleById);
router.post("/create", indexController.createArticle);

module.exports = router;
